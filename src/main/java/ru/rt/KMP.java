package ru.rt;

import java.util.Arrays;

public class KMP {

    public int find(String text, String pattern)
    {
        int[] pi = createPi(pattern);
        int j = 0;
        for (int i = 0; i < text.length(); ++i)
        {
            while (pattern.charAt(j) != text.charAt(i) && j > 0)
            {
                j = pi[j - 1];
            }
            if (pattern.charAt(j) == text.charAt(i))
            {
                j++;
                if (j == pattern.length())
                {
                    return i - j + 1;
                }
            }
            else
            {
                j = 0;
            }
        }
        return -1;
    }

    public int[] findAll(String text, String pattern)
    {
        int[] pi = createPi(pattern);
        int[] res = new int[text.length()];
        int size = 0;
        int j = 0;
        for (int i = 0; i < text.length(); ++i)
        {
            while (pattern.charAt(j) != text.charAt(i) && j > 0)
            {
                j = pi[j - 1];
            }
            if (pattern.charAt(j) == text.charAt(i))
            {
                j++;
                if (j == pattern.length())
                {
                    res[size] = i - j + 1;
                    size++;
                    j = pi[j - 1];
                }
            }
            else
            {
                j = 0;
            }
        }
        return Arrays.copyOfRange(res,0,size);
    }

    private int[] createPi(String pattern) {
        int[] pi = new int[pattern.length() + 1];

        pi[1] = 0;
        for (int q = 1; q < pattern.length(); q++) {
            int len = pi[q];
            while (len > 0 && pattern.charAt(len) != pattern.charAt(q)) {
                len = pi[len];
            }
            if (pattern.charAt(len) == pattern.charAt(q)) {
                len++;
            }
            pi[q + 1] = len;
        }

        return pi;
    }


}
