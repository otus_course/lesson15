package ru.rt;

public class BoyerMur {

    public int find(String text, String pattern) {
        if (text.length() < pattern.length()) {
            return -1;
        }

        int[] shift = createShift(pattern);
        int t = 0;
        int last = pattern.length() - 1;
        while (t < text.length() - last)
        {
            int p = last;
            while (p >= 0 && text.charAt(t + p) == pattern.charAt(p))
                p--;
            if (p == -1)
                return t;
            t += shift[text.charAt(t + last)];
        }
        return -1;
    }

    private int[] createShift(String pattern)
    {
        int[] shift = new int[256];
        for (int j = 0; j < shift.length; j++)
            shift[j] = pattern.length();
        for(int p = 0; p < pattern.length() - 1; p++)
            shift[pattern.charAt(p)] = pattern.length() - p - 1;
        return shift;
    }

}
